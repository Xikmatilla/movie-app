/**
 * App Reducers
 */
import { combineReducers } from "redux";

import MovieReducer from "./MovieReducer";
import CastPayload from "./CastReducer";


const reducers = combineReducers({


    movie: MovieReducer,
    cast:CastPayload


});

export default reducers;

