/**
 * Reducers
 */
import {
    GET_CAST,
    GET_CAST_SUCCESS,
    GET_CAST_FAIL,
    GET_RECOMMENDATION,
    GET_RECOMMENDATION_SUCCESS,
    GET_RECOMMENDATION_FAIL

} from "actions/types";

const INIT_STATE = {
    CastPayload: {},
    loading: false,
    Recommendations: {},
    loadingRec: false



};


export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_CAST:
            return {
                ...state,
                loading: true
            };

        case GET_CAST_SUCCESS:
            return {
                ...state,
                CastPayload: action.payload,
                loading: false
            };

        case GET_CAST_FAIL:
            return {
                ...state,
                loading: false
            };

        case GET_RECOMMENDATION:
            return {
                ...state,
                loadingRec: true
            };

        case GET_RECOMMENDATION_SUCCESS:
            return {
                ...state,
                Recommendations: action.payload,
                loadingRec: false
            };

        case GET_RECOMMENDATION_FAIL:
            return {
                ...state,
                loadingRec: false
            };



        default:
            return {...state};
    }
};
