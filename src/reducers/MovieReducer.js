/**
 * Reducers
 */
import {
    GET_MOVIE,
    GET_MOVIE_SUCCESS,
    GET_MOVIE_FAIL,
    GET_MOVIE_SLIDER,
    GET_MOVIE_SLIDER_SUCCESS,
    GET_MOVIE_SLIDER_FAIL,
    GET_MOVIE_ALL,
    GET_MOVIE_ALL_SUCCESS,
    GET_MOVIE_ALL_FAIL,
    GET_MOVIE_ID,
    GET_MOVIE_ID_SUCCESS,
    GET_MOVIE_ID_FAIL

} from "actions/types";

const INIT_STATE = {
    moviePayload: {},
    loadingMovie: false,
    sliderMove: [],
    loadingSlider: false,
    movieAll: {},
    loadingAll: false,
    movieAbout: {},
    loadingAbout: false,


};


export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_MOVIE:
            return {
                ...state,
                loadingMovie: true
            };

        case GET_MOVIE_SUCCESS:
            return {
                ...state,
                moviePayload: action.payload,
                loadingMovie: false
            };

        case GET_MOVIE_FAIL:
            return {
                ...state,
                loadingMovie: false
            };


        case GET_MOVIE_SLIDER:
            return {
                ...state,
                loadingSlider: true
            };

        case GET_MOVIE_SLIDER_SUCCESS:
            return {
                ...state,
                sliderMove: action.payload,
                loadingSlider: false
            };


        case GET_MOVIE_SLIDER_FAIL:
            return {
                ...state,
                loadingSlider: false
            };


        case GET_MOVIE_ALL:
            return {
                ...state,
                loadingAll: true
            };

        case GET_MOVIE_ALL_SUCCESS:
            return {
                ...state,
                movieAll: action.payload,
                loadingAll: false
            };


        case GET_MOVIE_ALL_FAIL:
            return {
                ...state,
                loadingAll: false
            };

        case GET_MOVIE_ID:
            return {
                ...state,
                loadingAbout: true
            };

        case GET_MOVIE_ID_SUCCESS:
            return {
                ...state,
                movieAbout: action.payload,
                loadingAbout: false
            };

        case GET_MOVIE_ID_FAIL:
            return {
                ...state,
                loadingAbout: false
            };


        default:
            return {...state};
    }
};
