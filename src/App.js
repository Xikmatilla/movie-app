import React, { useState, useEffect} from 'react';

import { Switch, Route, Redirect } from 'react-router-dom';



import './assets/css/index.scss';
import './App.css';

import HomePage from "containers/HomePage/HomPage";
import MoviesPage from "./containers/MoviesPage/MoviesPage";
import HeaderComponent from "./components/Header/HeaderComponent";
import AboutMoviePage from "./containers/AboutMovie/AboutMovie";




function App() {
  return (
      <div className="App">
          <HeaderComponent/>
          <Switch>
              <Route exact path='/' component={HomePage}  />
              <Route exact path='/movies' component={MoviesPage}  />
              <Route exact path='/movies/:id' component={AboutMoviePage}  />
          </Switch>
      </div>

  );
}

export default App;
