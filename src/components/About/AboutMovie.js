import React from 'react';
import img from '../../assets/image/about.jpg'
import config from "config";
import {cutString, genres} from "../../services";

const AboutMovieComponent = ({movie, loading}) => {


    return (
        <div className="about-movie-container">
            <div className="about-image-movie">
                <img src={`${config.API_IMAGE.large}/${movie.poster_path}`}  alt="img"/>
            </div>
            <div className="about-text-section">
                <div className="title-section">
                    <span>Title:</span>
                    <h1>{movie.original_title}</h1>
                </div>

                <div className="overview-section">
                    <span>Overview:</span>
                    <p>{movie.overview  && cutString(movie.overview, 350)}...</p>
                </div>
                <div className="data-section">
                    <span>Date of release:</span>
                    <h4>{movie.release_date}</h4>
                </div>
                <div className="budget-popularity">
                    <div className="budget-section">
                        <span>Budget:</span>
                        <h4>$ {movie.budget}</h4>
                    </div>
                        <div className="popularity-section">
                        <span>Popularity:</span>
                        <h4>{movie.popularity}</h4>
                    </div>
                </div>
                <div className="button-section-about">

                    {!loading && movie.genres && movie.genres.map((item, i) => (
                        <button key={item.id}>{item.name}</button>
                    ))}
                </div>
            </div>
        </div>
    )

};

export default AboutMovieComponent;