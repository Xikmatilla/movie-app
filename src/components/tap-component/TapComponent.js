import React from 'react';


const TapComponent = ({categoryKey, category, onHandlerMovie}) => {

    return (

        <div className="tap-containers">
            <div className="tap-section">
                <h1>
                    MOVIE
                </h1>
                <ul className="tap-block">
                    {
                        category.map((item, key) => (
                            <li key={key}
                                className={categoryKey === item.key ? 'active' : 'noActive'}
                                onClick={()=> onHandlerMovie(item.key )}
                            >#{item.title}</li>
                        ))
                    }
                </ul>
            </div>
        </div>
    )

};

export default TapComponent;