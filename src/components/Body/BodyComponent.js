import React from 'react';
import CardComponent from "../Card/CardComponent";
import { useHistory } from 'react-router-dom';


const BodyComponent = ({movie, loading}) => {
    const history = useHistory();


    const toPush = (id) => {
        history.push(`/movies/${id}`)
    };



    return(
        <div className="bod-container">
            <div className="body-section">
                <CardComponent movie={movie.results} loading={loading}
                               toPush={toPush}
                />
            </div>
            <div className="body-button-section">
                <button
                    onClick={() => history.push('/movies')}
                >VIEW ALL</button>
            </div>
        </div>
    )
};

export default BodyComponent;