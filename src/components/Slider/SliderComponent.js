import React, { useState } from 'react';

// react-id-swiper
import 'swiper/swiper-bundle.css';
import Swiper from 'react-id-swiper';
import imageBag from '../../assets/image/bag.jpg';
import config from "config";

import { cutString, genres } from "services";


const HeroSliderConfigs = {
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev"
    },
    effect: 'slide',
};

// slider component
const SliderComponent = ({loading, slider}) => {

    const [parallaxSwiper, setParallaxSwiper] = useState(null);
    const parallaxAmount = parallaxSwiper ? parallaxSwiper.width * 0.95 : 0;
    const parallaxOpacity = 0.5;
    return (
        <div
            style={{ backgroundImage: `url(${imageBag})`}}
            className="slider-root-styles">
            <div className="baground-slider">



                <div className="slider-container">
                    <Swiper {...HeroSliderConfigs} getSwiper={setParallaxSwiper}>

                        {
                            !loading && slider.length > 0 && slider.map((slid, key) => (

                                <div className="hero-slide" key={key}>
                                    <div
                                        className="slide-image"
                                        data-swiper-parallax={parallaxAmount}
                                        data-swiper-parallax-opacity={parallaxOpacity}
                                    >
                                        <img src={`${config.API_IMAGE.medium}/${slid.poster_path}`} alt="image1"></img>
                                    </div>
                                    <div className="text-section">
                                        <div className="button-section">
                                            {slid.genre_ids.map((item, i) => (
                                                <button key={i}>{genres(item)}</button>
                                            ))}
                                            <button>Action</button>
                                            <button>Action</button>
                                        </div>
                                        <div className="text-name">
                                            <h1>{slid.original_title && cutString(slid.original_title, 15)}...</h1>
                                            <p>
                                                {cutString(slid.overview, 150)}...</p>
                                            <ul className="movie-statistics">
                                                <li><span>Vote count: {slid.vote_count}</span></li>
                                                <li><span>Vote average:{slid.vote_average}</span></li>
                                                <li><span>Popularity:{slid.popularity}</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            ))
                        }


                    </Swiper>
                </div>
            </div>

        </div>

    );
};

export default SliderComponent;