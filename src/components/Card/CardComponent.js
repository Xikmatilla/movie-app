import React, {useState} from "react";

import 'swiper/swiper-bundle.css';
import config from "config";
import LoadingComponent from "../Loading/LoadingComponent";
import {cutString} from "../../services";


const CardComponent = ({movie, loading, toPush}) => {

    return(
        <div className="card-container">


            {movie ?  movie.map((item, key) => (

                <div key={key} className="card-block">
                    <div className="img-section">
                        <img src={`${config.API_IMAGE.medium}/${item.poster_path}`} alt="img"/>
                    </div>
                    <div className="text-section">
                        <div className="movie-name">
                            <span>{item.original_title && cutString(item.original_title, 15)}...</span>
                        </div>
                        <div className="movie-star">
                            <span>☆</span><span>{item.vote_average}</span>
                        </div>
                        </div>
                    <div className="button-section">
                        <button

                            onClick={() => toPush(item.id)}
                        >VIEW</button>
                    </div>
                </div>
            )) : <LoadingComponent/>}

            </div>
    )

};

export default CardComponent;