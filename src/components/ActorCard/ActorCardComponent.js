import React, {useState} from 'react';
import img from '../../assets/image/about.jpg'
import config from "config";



const ActorCardComponent = ({item}) => {
 const { cast } = item;


    const [showNumber, setShowNumber] = useState(4);
    const [show, setShow] = useState(false);

    const onHandlerShow = () => {

        if (cast.length > showNumber) {
            setShowNumber(showNumber + showNumber)
        }else {
            setShow(true);
        }
    };


    const onHandlerHidden = () => {

        if (showNumber !== 4) {
            setShowNumber(showNumber - 4)
        }else {
            setShow(false);

        }
    };





    return (
        <div className="actor-container">
            {!show ? <button className="show-for-actor"
            onClick={onHandlerShow}
            >Show All</button> : <button className="show-for-actor"
                                         onClick={onHandlerHidden}
            >Hidden</button> }
           <div className="actor-blog">
               {
                   cast && cast.filter((i, index )=> index < showNumber).map((item, key) => (
                       <div className="actor-img-section">
                           <img src={`${config.API_IMAGE.medium}/${item.profile_path}`}  alt="actor"/>
                           <div className="actor-text-section">
                               <h4>{item.name}</h4>
                               {/*<span>Zeke</span>*/}
                           </div>
                       </div>
                   ))
               }


           </div>
        </div>
    )
};

export default ActorCardComponent;