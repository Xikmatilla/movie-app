import React, {useEffect, useState} from 'react';

import {useDispatch, useSelector} from "react-redux";
import { getMovie, getMovieSlider } from "actions";

import { category } from "./category/category";

import HeaderComponent from "../../components/Header/HeaderComponent";
import SliderComponent from "../../components/Slider/SliderComponent";
import TapComponent from "../../components/tap-component/TapComponent";
import BodyComponent from "../../components/Body/BodyComponent";


const HomePage = () => {


    const [categoryKey, setCategoryKey] = useState('now_playing');


    const dispatch = useDispatch();
    const { loading, slider, moviePayload, loadingMovie} = useSelector(({movie })=> ({
        loading: movie.loadingSlider,
        slider: movie.sliderMove,
        moviePayload: movie.moviePayload,
        loadingMovie: movie.loadingMovie


    }));



    useEffect(()=> {
        dispatch(getMovieSlider({
            payload: 3
        }));

        dispatch(getMovie({
            payload: {
                url: categoryKey,
                page: 3

            }
        }));


        window.scrollTo(0, 0)
    }, []);


    const onHandlerMovie = (key) => {
        setCategoryKey(key);
        dispatch(getMovie({
            payload: {
                url: key,
                page: 3

            }
        }));
    };





    return (
        <div>
            <SliderComponent slider={slider} loading={loading}/>
            <TapComponent
                categoryKey={categoryKey}
                category={category}
                onHandlerMovie={onHandlerMovie}/>
            <BodyComponent
                movie={moviePayload}
                loading={loadingMovie}
            />
        </div>
    )


};


export default HomePage;