export const category = [
    {
        key: 'now_playing',
        title: 'NOW PLAYING'
    },
    {
        key: 'popular',
        title: 'POPULAR'
    },{
        key: 'upcoming',
        title: 'COMING SOON'
    },{
        key: 'top_rated',
        title: 'TOP RATED'
    },
];
