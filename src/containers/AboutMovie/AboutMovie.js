import React, {useEffect, useState} from 'react';
import { useParams } from 'react-router-dom'
import {useDispatch, useSelector} from "react-redux";
import { useHistory } from 'react-router-dom';

import { getMovieId, getCast, getRecommendations } from "actions";


import CardComponent from "../../components/Card/CardComponent";
import { getMovieAll} from "actions";
import AboutMovieComponent from "../../components/About/AboutMovie";
import ActorCardComponent from "../../components/ActorCard/ActorCardComponent";
import LoadingComponent from "../../components/Loading/LoadingComponent";
import Recommendations from "../../components/Recommendations/Recommendations";

const AboutMoviePage = () => {
    const dispatch = useDispatch();
    const {id} = useParams();
    const history = useHistory();



    const { loading, movie, moviePayload, loadingMovie} = useSelector(({movie })=> ({
        loading: movie.loadingAbout,
        movie: movie.movieAbout,
        moviePayload: movie.moviePayload,
        loadingMovie: movie.loadingMovie

    }));


    const { loadingCast, cast, recommendations, loadingRec} = useSelector(({ cast })=> ({
        loadingCast: cast.loading,
        cast: cast.CastPayload,
        recommendations: cast.Recommendations,
        loadingRec: cast.loadingRec
    }));

    const toPush = (idIetm) => {
        dispatch(getMovieId({
            payload: idIetm
        }));
        dispatch(getCast({
            payload: idIetm
        }));


        dispatch(getRecommendations({
            payload: id
        }));

        history.push(`/movies/${id}`);

        window.scrollTo(0, 0)

    };




    useEffect(()=> {

        dispatch(getMovieId({
            payload: id
        }));


        dispatch(getCast({
            payload: id
        }));

        dispatch(getRecommendations({
            payload: id
        }));
        window.scrollTo(0, 0)

    }, []);


    return(
        <div className="">
            <AboutMovieComponent movie={movie} loading={loading}/>

            {!loadingCast && cast ? <ActorCardComponent item={cast} /> : <LoadingComponent/> }


            <Recommendations movie={recommendations} loading={loadingRec} toPush={toPush} />
        </div>
    )
};

export default AboutMoviePage;