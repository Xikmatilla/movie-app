import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import { useHistory } from 'react-router-dom';

import CardComponent from "../../components/Card/CardComponent";
import { getMovieAll} from "actions";
import HeaderComponent from "../../components/Header/HeaderComponent";

const MoviesPage = () => {

    const history = useHistory();
    const dispatch = useDispatch();
    const [page, setPage] = useState(1);



    const { loading, movie } = useSelector(({movie })=> ({
        movie: movie.movieAll,
        loading: movie.loadingAll


    }));

    useEffect(()=> {
        dispatch(getMovieAll({
            payload: page
        }));
        window.scrollTo(0, 0)
    }, []);


    const handlerPage = () => {
        setPage(page+ 1);

        if (movie.total_pages > page) {
            dispatch(getMovieAll({
                payload: page + 1
            }));
        }else {
            setPage(1);
            dispatch(getMovieAll({
                payload: 1
            }));
        }

    };



    const toPush = (id) => {
        history.push(`/movies/${id}`)
    };

    return(
        <div className="all-movies-page">

            <div className="bod-container">
                <div className="body-section">
                    <h1>NOW_PLAYING - MOVIES</h1>
                    <CardComponent movie={movie.results} loading={loading} toPush={toPush}/>
                </div>
                <div className="body-button-section">
                    <button onClick={handlerPage}>LOAD MORE</button>
                </div>
            </div>
        </div>
    )
};

export default MoviesPage;