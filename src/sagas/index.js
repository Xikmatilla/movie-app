/**
 * Root Sagas
 */
import { all, fork } from "redux-saga/effects";



import workerMovieSaga from "./watchers/MovieSaga";

export default function* rootSaga() {
    yield all([

        fork(workerMovieSaga),
    ]);
}
