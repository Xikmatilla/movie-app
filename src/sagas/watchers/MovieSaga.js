/* eslint-disable no-empty */
import {takeLatest, call, put} from "redux-saga/effects";
import * as types from "actions/types";
import {api} from "services";


import config from 'config';
import {GET_CAST} from "../../actions/types";


function* workerMovieSlider({payload}) {

    try {
        const {data} = yield call(api.request.get, `/${'trending'}/all/day?per-page=${payload}&sort=-id&_f=json&api_key=${config.API_KEY}`);
        yield put({
            type: types.GET_MOVIE_SLIDER_SUCCESS,
            payload: data.results
        });
    } catch (error) {
        yield put({
            type: types.GET_MOVIE_SLIDER_FAIL,
            payload: error.response.data.message
        });
    }
}

function* workerMovie({payload}) {

    try {
        const {data} = yield call(api.request.get, `/movie/now_playing?page=${payload}&per-page=3&sort=-id&_f=json&api_key=${config.API_KEY}`);
        yield put({
            type: types.GET_MOVIE_ALL_SUCCESS,
            payload: data
        });
    } catch (error) {
        yield put({
            type: types.GET_MOVIE_ALL_FAIL,
            payload: error.response.data.message
        });
    }
}


function* workerMovieCategory({payload}) {

    try {
        const {data} = yield call(api.request.get, `/movie/${payload.url}?per-page=${payload.page}&sort=-id&_f=json&api_key=${config.API_KEY}`);
        yield put({
            type: types.GET_MOVIE_SUCCESS,
            payload: data
        });
    } catch (error) {
        yield put({
            type: types.GET_MOVIE_FAIL,
            payload: error.response.data.message
        });
    }
}


function* workerMovieId({payload}) {

    try {
        const {data} = yield call(api.request.get, `/movie/${payload}?&_f=json&api_key=${config.API_KEY}`);
        yield put({
            type: types.GET_MOVIE_ID_SUCCESS,
            payload: data
        });
    } catch (error) {
        yield put({
            type: types.GET_MOVIE_ID_FAIL,
            payload: error.response.data.message
        });
    }
}





function* workerGetCast({payload}) {

    try {
        const {data} = yield call(api.request.get, `/movie/${payload}/credits?&_f=json&api_key=${config.API_KEY}`);

        yield put({
            type: types.GET_CAST_SUCCESS,
            payload: data
        });
    } catch (error) {
        yield put({
            type: types.GET_CAST_FAIL,
            payload: error.response.data.message
        });
    }
}

function* workerGetRecommendations({payload}) {

    try {
        const {data} = yield call(api.request.get, `/movie/${payload}/recommendations?per-page=3&sort=-id&_f=json&api_key=${config.API_KEY}`);
        yield put({
            type: types.GET_RECOMMENDATION_SUCCESS,
            payload: data
        });
    } catch (error) {
        yield put({
            type: types.GET_RECOMMENDATION_FAIL,
            payload: error.response.data.message
        });
    }
}

export default function* workerMovieSaga() {

    yield takeLatest(types.GET_MOVIE, workerMovieCategory);
    yield takeLatest(types.GET_MOVIE_SLIDER, workerMovieSlider);
    yield takeLatest(types.GET_MOVIE_ALL, workerMovie);
    yield takeLatest(types.GET_MOVIE_ID, workerMovieId);
    yield takeLatest(types.GET_CAST, workerGetCast);
    yield takeLatest(types.GET_RECOMMENDATION, workerGetRecommendations);

}
