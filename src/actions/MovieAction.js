import {
    GET_MOVIE,
    GET_MOVIE_SLIDER,
    GET_MOVIE_ALL,
    GET_MOVIE_ID,
    GET_CAST,
    GET_RECOMMENDATION
} from "./types";




// getMovie Actions
export const getMovie = ({payload}) => ({
    type: GET_MOVIE,
    payload: payload
});


// getMovieSlider Actions
export const getMovieSlider = ({payload}) => ({
    type: GET_MOVIE_SLIDER,
    payload: payload
});

// getMovieAll Actions
export const getMovieAll = ({payload}) => ({
    type: GET_MOVIE_ALL,
    payload: payload
});

// getMovieId Actions
export const getMovieId = ({payload}) => ({
    type: GET_MOVIE_ID,
    payload: payload
});

// getCastT Actions
export const getCast= ({payload}) => ({
    type: GET_CAST,
    payload: payload
});

// getRecommendations Actions
export const getRecommendations = ({payload}) => ({
    type: GET_RECOMMENDATION,
    payload: payload
});

