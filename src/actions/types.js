
export const GET_MOVIE = "GET_MOVIE";
export const GET_MOVIE_SUCCESS  = "GET_MOVIE_SUCCESS";
export const GET_MOVIE_FAIL  = "GET_MOVIE_FAIL";


export const GET_MOVIE_SLIDER = "GET_MOVIE_SLIDER";
export const GET_MOVIE_SLIDER_SUCCESS  = "GET_MOVIE_SLIDER_SUCCESS";
export const GET_MOVIE_SLIDER_FAIL  = "GET_MOVIE_SLIDER_FAIL";


export const GET_MOVIE_ALL = "GET_MOVIE_ALL";
export const GET_MOVIE_ALL_SUCCESS  = "GET_MOVIE_ALL_SUCCESS";
export const GET_MOVIE_ALL_FAIL = "GET_MOVIE_ALL_FAIL";

export const GET_MOVIE_ID = "GET_MOVIE_ID";
export const GET_MOVIE_ID_SUCCESS = "GET_MOVIE_ID_SUCCESS";
export const GET_MOVIE_ID_FAIL = "GET_MOVIE_ID_FAIL";



export const GET_CAST = "GET_CAST";
export const GET_CAST_SUCCESS = "GET_CAST_SUCCESS";
export const GET_CAST_FAIL = "GET_CAST_FAIL";



export const GET_RECOMMENDATION = "GET_RECOMMENDATION";
export const GET_RECOMMENDATION_SUCCESS = "GET_RECOMMENDATION_SUCCESS";
export const GET_RECOMMENDATION_FAIL = "GET_RECOMMENDATION_FAIL";
